# @author Taylor-Jayde Blackstone <t.blackstone@inspyre.tech>
# @since 1.0
#
# A library to hold version information for the installer
# and the target eveIRC Bot it's working to install
#
# @see EveIRCInstaller#Version
module EveIRCInstaller
  VERSION     = "1.0a1.08"
  TARGET      = '6.10a'
  PRE_RELEASE = true
end
